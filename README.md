# TP Back BRUN-GIGLIO Alexandre B3 Info

## Install 

Run : 
- `composer install`.
- `symfony check:requirements` to check if symfony project coulb be start. 
- `php bin/console make:migration` and `php bin/console doctrine:migrations:migrate` to create migration files. 
- `php bin/console doctrine:fixtures:load ` to create default data for the projet. 

Fixtures will create : 
> Admin user : admin@admin.fr / admin. 

> user : user@user.fr / user. 

> One article, you will able edit it or delete it. 

## Development server

Run `symfony server:start` for a dev server. Navigate to `http://127.0.0.1:8000/`.

## Achievements

- [x] Accueil. 
- [x] S'inscrire sur le blog. 
- [x] Voir les articles du blog. 
- [x] Trier les articles. 
- [x] Voir un article en détails. 
- [x] Rôle admin.
- [x] Pannel d'adminnistration pour les articles. 
- [x] Pannel d'administration pour les commentaires. 
- [x] Gérer le statut d'un commentaire. 

## MCD 

[mcd.png](public/img/mcd.png)

## Problèmes rencontrés 
La technologie utilisé m'a pas mal démotivé sur ce projet. Celà est du à mes préférences en tant que développeur. 
J'ai également rencontré des problèmes sur les commentaires où je n'ai pas réussis à bien les implanter. 
Cependant j'ai pu grâce à ce projet, me confronter à une techno que je ne maîtrisais pas. 
