<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class GlobalController extends AbstractController
{

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboard(): Response
    {
        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_home');
        } elseif ($this->container->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('homepage');
        } else {
            return $this->redirectToRoute('homepage');
        }
    }

    /**
     * @Route("admin/filters/", name="admin_filters")
     */

    public function filterArticle(ArticleRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        if (isset($_GET['filter'])) {
            $filter = $_GET['filter'];
        } else {
            throw $this->createNotFoundException('Cette page n\'existe pas');
        }

        $article = $repository->articleFilter($filter);

        // Paginate the results of the query
        $articles = $paginator->paginate(
            // Doctrine Query, not results
            $article,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            10
        );

        if (!$articles) {
            throw $this->createNotFoundException('Cette page n\'existe pas');
        }

        return $this->render('admin_dashboard/articles.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("admin/filtersComment/", name="admin_filtersCom")
     */

    public function filterCommentary(CommentRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        if (isset($_GET['filter'])) {
            $filter = $_GET['filter'];
        } else {
            throw $this->createNotFoundException('Cette page n\'existe pas');
        }

        $comment = $repository->commentFilter($filter);

        // Paginate the results of the query
        $comments = $paginator->paginate(
            // Doctrine Query, not results
            $comment,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            10
        );

        if (!$comment) {
            throw $this->createNotFoundException('Cette page n\'existe pas');
        }

        return $this->render('admin_dashboard/comments.html.twig', [
            'comments' => $comments,
        ]);
    }
}
