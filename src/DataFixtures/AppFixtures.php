<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Bookmark;
use App\Entity\Comment;
use App\Entity\Role;
use App\Entity\Share;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /** @var UserPasswordEncoderInterface */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {

        $userRole = new Role();
        $userRole->setLabel("ROLE_USER");
        $manager->persist($userRole);

        $adminRole = new Role();
        $adminRole->setLabel("ROLE_ADMIN");
        $manager->persist($adminRole);

        $manager->flush();

        $userAdmin = new User();
        $userAdmin->setEmail('admin@admin.fr')
            ->setPassword($this->encoder->encodePassword($userAdmin, 'admin'))
            ->setFirstName('admin')
            ->setLastName('admin')
            ->addUserRole($userRole)
            ->addUserRole($adminRole);
        $manager->persist($userAdmin);

        $user = new User();
        $user->setEmail('user@user.fr')
            ->setPassword($this->encoder->encodePassword($user, 'user'))
            ->setFirstName('user')
            ->setLastName('user')
            ->addUserRole($userRole);
        $manager->persist($user);

        for ($i = 1; $i < 20; $i++) {
            $article = new Article();
            $article->setTitle('Mon article' .  $i);
            $article->setSubTitle('J\'avais hâte');
            $article->setUser($userAdmin);
            $article->setContent('Voici les premières lignes de mon blog !');
            $article->setCreationDate(new DateTime('now -' . $i . ' hour'));
            $article->setPublic(true);
            $article->setState('validated');
            $article->setPicture('https://www.breizhmasters.fr/get_image/500-500/data/upload/creer-un-site-internet-sur-mes-m5rf1.jpg');
            $article->setCategory('informatique');
            $article->setReadingTime(10);
            $manager->persist($article);

            // $comment = new Comment();
            // $comment->setContent('Super article ! merci');
            // $comment->setCreationDate(new DateTime('now -' . $i . ' hour'));
            // $comment->setState('wainting');
            // $comment->setPrivacy('');
            // $comment->setArticle($article);
            // $comment->setUser($user);

            // $manager->persist($comment);
        }
        $manager->flush();
    }
}
